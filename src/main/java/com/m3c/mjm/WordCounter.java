package com.m3c.mjm;

import java.io.*;
import java.util.*;

public class WordCounter {

    public WordCounter(){

    }


    public BufferedReader getReader(String filePath) {
        BufferedReader in = null;

        File testFile = new File(filePath);

        try {
           in = new BufferedReader(new FileReader(testFile));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return in;
    }

    public String countWords(BufferedReader input1){
        int wordCounter=0;
        String mostCommonLineWord="DEFAULT";
        String [] lineData;
        String line = null;
        try {
            line = input1.readLine();


            Map<String,Integer> words_count = new HashMap<String,Integer>();

            while (line!=null) {

                lineData = line.toLowerCase().split("[^A-Za-z0-9-']"); //decides where to  split each line and puts each split into lineData array.

                for(int i=0; i<lineData.length ;i++) {

                    if (lineData[i] != null) {

                        String s = lineData[i];
                        if (words_count.keySet().contains(s)) {
                            Integer count = words_count.get(s) + 1;
                            words_count.put(s, count);
                        } else {
                            words_count.put(s, 1);
                        }
                    }
                }
                line=input1.readLine();//reads next line
                //System.out.println(lineData.length); //counts splits in line
            }

            Integer frequency = null;
            String mostFrequent = null;
            for(String s : words_count.keySet())
            {
                Integer i = words_count.get(s);
                if(frequency == null)
                    frequency = i;
                if(i > frequency)
                {
                    frequency = i;
                    mostFrequent = s;
                }
            }
            System.out.println("The word "+ mostFrequent +" occurred "+ frequency +" times");
        } catch (IOException e) {
            e.printStackTrace();
        }




        if (line == null){
            return null;
        }
        String wut = " ";
        return wut;
    }
}
