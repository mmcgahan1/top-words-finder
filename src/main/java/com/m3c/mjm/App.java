package com.m3c.mjm;

public class App 
{
    public static void main( String[] args )
    {
        double begin, end, runtime;
        WordCounter tester = new WordCounter();
        begin = System.nanoTime();
        tester.countWords(tester.getReader("./resources/aLargeFile"));
        end = System.nanoTime();



        runtime = (end-begin);
        System.out.println(runtime*0.000000001+" seconds.");
        System.out.println(runtime*0.000001+" milliseconds.");
        System.out.println(runtime+" nanoseconds.");
    }
}
